#!/usr/bin/env python
# generate 45 angle slope cut along X/Z axis

import pylab
bit_d = 0.25
bit_r = bit_d/2
safety_z = 0.15
material_y = 3.5
material_z = 0.75
feed = 50
material_x = 17.25
start_position = False
last_x = 0
last_y = 0

def init():
    print ("G90") # absolute coordinates)
    print ("G20") # inches
    # put something here to "home" z axis?

def spindle(on=False):
    print ("M3 S12000" if on else "M5")

def go(x, y, z, g=1):
    global start_position, safety_z, feed
    if not start_position:
        print ("G0 X{:.6f} Y{:.6f} Z{:.6f}".format(x, y, safety_z))
        start_position = True
    if g == 0:
        print ("G0 X{:.6f} Y{:.6f} Z{:.6f}".format(x, y, z))
    else:
        print ("G{:d} X{:.6f} Y{:.6f} Z{:.6f} F{:d}".format(g, x, y, z, feed))
        # ("G" + str(g) + " X" + str(x) + " Y" + str(y) + " Z" + str(z) + " F" + str(feed))
    last_x = x
    last_y = y

def safe():
    print ("G0 Z{:.6f}".format(safety_z))
    start_position = False

def done():
    safe()
    spindle()

init()
spindle(True)
for z in pylab.frange(0.1, material_z, 0.02):
    x = material_x - (material_z - z) + bit_r
    go(x, 0 + bit_r, -z)
    go(x, material_y - bit_r, -z)

done()
