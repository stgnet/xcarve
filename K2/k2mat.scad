// Travato 59K V2 Bath Mat (Cedar Shower Deck)

$fn=128;

mm = 25.4;
basin_y = 22.23 * mm;
basin_x_main = 25.38 * mm;
basin_x_extra = 26.50 * mm;

board_thick = .7 * mm;

boards = 7;

board_y = basin_y / boards;

runner_x = 3 * mm;
runner_y = basin_y;

board_notch = .3 * mm;

slot_width = 0.5 * mm;
slot_gap = 0.5 * mm;

corner_r = 0.75 * mm;

// two runners offset from center
runner_center_offset = 7 * mm;
basin_x_center = basin_x_main / 2;
runner_positions = [basin_x_center - runner_center_offset - runner_x/2,
                    basin_x_center + runner_center_offset - runner_x/2];

slot_positions = [
                    [0+slot_gap, runner_positions[0]-slot_gap],
                    [runner_positions[0]+runner_x+slot_gap, runner_positions[1]-slot_gap],
                    [runner_positions[1]+runner_x+slot_gap, basin_x_main-slot_gap]
                 ];
                  
echo("Board Y = ",board_y, board_y/mm);

module board(number) {
    board_x = number==0 ? basin_x_extra : basin_x_main;
    
    difference() {
        cube([board_x, board_y, board_thick]);
        
        // notches for runners
        for (x = runner_positions) {
            translate([x, 0, 0])
                cube([runner_x, board_y, board_notch]);
        }
        sides=number==0?[board_y]:number==boards-1?[0]:[0,board_y];
        // slots for draining
        for (side = sides) {
            for (slot = slot_positions) {
                translate([slot[0], side, 0]) {
                    hull() {
                        translate([slot_width/2, 0, 0])
                            cylinder(d=slot_width, h=board_thick, center=false);
                        translate([slot[1]-slot[0]-slot_width/2, 0, 0])
                            cylinder(d=slot_width, h=board_thick, center=false);
                    }
                }
            }
        }
        corners=number==0?[[0,0,corner_r,corner_r],[board_x,0,board_x-corner_r,corner_r]]:
                number==boards-1?[[0,board_y,corner_r,board_y-corner_r],[board_x,board_y,board_x-corner_r,board_y-corner_r]]:
                [];
        for (corner=corners) {
            difference() {
                translate([corner[0], corner[1], board_thick]) cube([corner_r*2, corner_r*2, board_thick*2], center=true);
                translate([corner[2], corner[3], board_thick]) sphere(r=corner_r);
            }
        }
        ysides=number==0?[[0,corner_r]]:number==boards-1?[[board_y,board_y-corner_r]]:[];
        for (yside=ysides) {
            difference() {
                translate([board_x/2,yside[0],board_thick]) cube([board_x, corner_r*2, board_thick*2], center=true);
                translate([0,yside[1],board_thick]) rotate([0,90,0]) cylinder(r=corner_r, h=board_x, center=false);
            }
        }
        xsides=[[0,corner_r],[board_x,board_x-corner_r]];
        for (xside=xsides) {
            difference() {
                translate([xside[0],board_y/2,board_thick]) cube([corner_r*2,board_y,board_thick*2], center=true);
                translate([xside[1],0,board_thick]) rotate([-90,0,0]) cylinder(r=corner_r, h=board_y, center=false);
            }
        }
    }
}

for (n = [0:6]) {
    translate([0, n*board_y+n, 0])
        board(n);
}

/* vim: set ts=4 sw=4 sts=4 noet : */
