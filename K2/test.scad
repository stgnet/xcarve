$fn=128;

corner_r=15;

// small sample board to test pycam template
difference() {
    cube([50, 50, 15]);
    translate([50, 30, 0])
        cylinder(r=10, h=15, center=false);
    difference() {
        translate([0, 0, 0]) cube([corner_r*2, corner_r*2, 40], center=true);
        translate([corner_r, corner_r, 0]) sphere(r=corner_r);
    }

    difference() {
        translate([25,0,0]) cube([50, corner_r*2, 40], center=true);
        translate([0,corner_r,0]) rotate([0,90,0]) cylinder(r=corner_r, h=50, center=false);
    }
    difference() {
        translate([0,25,0]) cube([corner_r*2,50,40], center=true);
        translate([corner_r,0,0]) rotate([-90,0,0]) cylinder(r=corner_r, h=50, center=false);
    }

}
/* vim: set ts=4 sw=4 sts=4 noet : */
