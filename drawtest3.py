import math
from graphics import *

POINTS = 5
WIDTH, HEIGHT = 600, 600

def main():
    win = GraphWin('Star', WIDTH, HEIGHT)
    win.setCoords(-WIDTH/2, -HEIGHT/2, WIDTH/2, HEIGHT/2)
    win.setBackground('White')

    vertices = []

    length = min(WIDTH, HEIGHT) / 2

    theta = -math.pi / 2
    delta = 4 / POINTS * math.pi

    for _ in range(POINTS):
        vertices.append(Point(length * math.cos(theta), length * math.sin(theta)))
        theta += delta

    # Use Polygon object to draw the star
    star = Polygon(vertices)
    star.setFill('darkgreen')
    star.setOutline('lightgreen')
    star.setWidth(4)  # width of boundary line
    star.draw(win)

    win.getMouse()
    win.close()

main()