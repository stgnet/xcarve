max_x = 750
max_y = 800

step = 50

print ("M3 S12000")
print ("G90")
print ("G21")
for y in range(0, max_y+1, step):
    print ("G0 X"+str(0)+" Y"+str(y))
    print ("G0 X"+str(max_x)+" Y"+str(y))
print ("M5")
print ("G0 X0 Y0")
