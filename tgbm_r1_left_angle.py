# generate 45 angle slope cut along X/Z axis
import pylab
bit_d = 0.25
bit_r = bit_d/2
safety_z = 0.15
material_y = 3.5
material_z = 0.75
feed = 50

def go(x, y, z, g=1):
    if g == 0:
        print "G" + str(g) + " X" + str(x) + " Y" + str(y) + " Z" + str(z)
    else:
        print "G" + str(g) + " X" + str(x) + " Y" + str(y) + " Z" + str(z) + " F" + str(feed)

safe = False
print "G90"
print "G20"
print "G0 Z" + str(safety_z)
print "M3 S12000"
for z in pylab.frange(0, material_z, 0.01):
    x = (material_z - z) - bit_r
    if x < bit_r: break
    if not safe:
        go(x, 0 + bit_r, safety_z, 0)
        safe = True
    go(x, 0 + bit_r, -z)
    go(x, material_y - bit_r, -z)
    last_x = x
    last_y = material_y - bit_r

go(last_x, last_y, safety_z, 0)
print "M5"
